import { readable } from 'svelte/store';

import { prayersCalc } from 'prayer-timetable-lib'; // ,dayCalc
// import { prayersCalc } from '../helpers/prayerTimetableCalc'; // ,dayCalc
import timetable from '../config/timetable';
// import defsettings from '../config/settings';
import { settingsStore } from './settingsStore';
// import { updateStore } from './updateStore';
// import checkOverlay from './actions/checkOverlay';

let settings;
const unsubscribeSettings = settingsStore.subscribe(value => {
  // console.log(value);
  settings = value;
});

if (settings.join === '0') settings.join = false;

const prayersGet = prayersCalc(timetable, settings, settings.jamaahShow);

// first argument for readable is default - initial value
export const prayersStore = readable(prayersGet, function start(set) {
  const interval = setInterval(() => {
    const calc = prayersCalc(timetable, settings, settings.jamaahShow);
    // console.log(settings.jamaahoffsets[2]);
    // console.log(settings.jamaahoffsets[4]);
    set(calc);

    // console.log(prayersGet.prayers.today[2].jtime);
    // console.log(prayersGet.prayers.today[4].jtime);

    // const eventTitle = checkOverlay(settings, calc.current);
    // updateStore.update(n => {
    //   n.event = eventTitle;
    //   return n;
    // });
    // console.log('ticked', prayersGet.now)
  }, 1000);

  return function stop() {
    clearInterval(interval);
    unsubscribeSettings();
  };
});
