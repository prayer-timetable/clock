module.exports = {
  prefix: '',
  important: false,
  separator: ':',

  theme: {
    colors: {
      transparent: 'rgba(0,0,0,0)',
      black: {
        o10: 'rgba(0,0,0,.1)',
        o25: 'rgba(0,0,0,.25)',
        o50: 'rgba(0,0,0,.5)',
        o75: 'rgba(0,0,0,.75)',
        o90: 'rgba(0,0,0,.9)',
        o: 'rgba(0,0,0)',
      },
      white: {
        o25: 'rgba(255,255,255,.25)',
        o50: 'rgba(255,255,255,.5)',
        o75: 'rgba(255,255,255,.75)',
        o90: 'rgba(255,255,255,.9)',
        o: 'rgb(255,255,255)',
      },
      red: {
        o25: 'rgba(190, 30, 46,.25)',
        o50: 'rgba(190, 30, 46,.5)',
        o75: 'rgba(190, 30, 46,.75)',
        o90: 'rgba(190, 30, 46, .9)',
        o: 'rgb(190, 30, 46)',
      },
      orange: {
        o25: 'rgba(175,75,25,.25)',
        o50: 'rgba(175,75,25,.5)',
        o75: 'rgba(175,75,25,.75)',
        o90: 'rgba(175,75,25, .9)',
        o: 'rgb(175,75,25)',
      },
      blue: {
        o25: 'rgba(37, 52, 67,.25)',
        o50: 'rgba(37, 52, 67,.5)',
        o75: 'rgba(37, 52, 67,.75)',
        o90: 'rgba(37, 52, 67, .9)',
        o: 'rgb(37, 52, 67)',
      },
      blue2: {
        o25: 'rgba(133, 147, 159,.25)',
        o50: 'rgba(133, 147, 159,.5)',
        o75: 'rgba(133, 147, 159,.75)',
        o90: 'rgba(133, 147, 159, .9)',
        o: 'rgb(133, 147, 159)',
      },
      grey: {
        o25: 'rgba(150, 150, 150,.25)',
        o50: 'rgba(150, 150, 150,.5)',
        o75: 'rgba(150, 150, 150,.75)',
        o90: 'rgba(150, 150, 150, .9)',
        o: 'rgb(150, 150, 150)',
      },
      grey2: {
        o25: 'rgba(245,245,245,.25)',
        o50: 'rgba(245,245,245,.5)',
        o75: 'rgba(245,245,245,.75)',
        o90: 'rgba(245,245,245, .9)',
        o: 'rgb(245,245,245)',
      },
      purple: {
        o25: 'rgba(68,14,98,.25)',
        o50: 'rgba(68,14,98,.5)',
        o75: 'rgba(68,14,98,.75)',
        o90: 'rgba(68,14,98, .9)',
        o: 'rgb(68,14,98)',
      },
    },
    extend: {
      spacing: {
        s: '1.875rem',
        '14': '3.5rem',
        '36': '9rem',
        '72': '18rem',
        '84': '21rem',
        '96': '24rem',
      },
      inset: {
        '-16': '-4rem',
        '-4': '-1rem',
        '1/2': '50%',
      },
      zIndex: {
        '100': 100,
      },
    },
    // height: {
    //     '36': '9rem',
    //     '72': '18rem',
    //     '84': '21rem',
    //     '96': '24rem',
    // },
  },
  variants: {},
  // variants: {
  //     backgroundColor: [
  //         'responsive',
  //         'group-hover',
  //         'first',
  //         'last',
  //         'odd',
  //         'even',
  //         'hover',
  //         'focus',
  //         'active',
  //         'visited',
  //         'disabled',
  //     ],
  // },
  corePlugins: {},
  plugins: [],
};

// elasr: {
//   red: "rgb(190, 30, 46)",
// },#
// blue
// rgb(37, 52, 67);
// purple
// 68,14,98

// module.exports = {
//     theme: {
//         extend: {},
//     },
//     variants: {},
//     plugins: [],
// }
