// const tailwindcss = require('tailwindcss')
const cssnano = require('cssnano');
// const cssvariables = require('postcss-css-variables');
const autoprefixer = require('autoprefixer');
// const cssvariables = require('postcss-nested');

const purgecss = require('@fullhuman/postcss-purgecss')({
  content: ['./src/**/*.svelte', './src/**/*.html', './src/**/*.svg', './src/**/*.js'],
  defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || [],
});
const postcssFontPath = require('postcss-fontpath')({
  checkFiles: true,
  ie8Fix: true,
});
const postcssPresetEnv = require('postcss-preset-env')({ stage: 0 }); // nesting and auto-prefixing

module.exports = {
  plugins: [
    postcssPresetEnv,
    autoprefixer,
    // cssvariables,
    // cssvariables({
    //   variables: {
    //     '--some-var': '100px',
    //     '--other-var': {
    //       value: '#00ff00',
    //     },
    //     '--important-var': {
    //       value: '#ff0000',
    //       isImportant: true,
    //     },
    //   },
    // }),
    // tailwindcss('./tailwind.config.js'),
    ...(process.env.NODE_ENV === 'production' ? [purgecss, cssnano, postcssFontPath] : []),
  ],
};
