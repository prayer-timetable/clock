// /*eslint-disable*/

import { toHijri } from 'hijri-converter'; // toGregorian
import { addHours, getMinutes, getHours, getSeconds } from 'date-fns';

const wordCount = str => str.split(' ').filter(n => n !== '').length;

// const prependZero = unit => {
//   if (unit < 10) {
//     return `0${unit}`;
//   }
//   return `${unit}`;
// };

const prependZero = value => (value < 10 ? `0${value}` : `${value}`);

// returns {hy, hm, hd}
const hijriConvert = now => {
  const { year, month, day } = {
    year: now.getFullYear(),
    month: now.getMonth(),
    day: now.getDate(),
  };
  // const result = toHijri(1987, 3, 1)

  const hijri = toHijri(year, month, day);
  // console.log(hijri.hm)

  return hijri;
};

// returns '1 Muharram 1440'
const hijriDate = now => {
  const hijri = hijriConvert(now);

  let month;

  switch (hijri.hm) {
    case 1:
      month = 'Muharram';
      break;
    case 2:
      month = 'Safar';
      break;
    case 3:
      month = 'Rabi’ al-awwal';
      break;
    case 4:
      month = 'Rabi’ al-thani';
      break;
    case 5:
      month = 'Jumada al-awwal';
      break;
    case 6:
      month = 'Jumada al-thani';
      break;
    case 7:
      month = 'Rajab';
      break;
    case 8:
      month = 'Sha’ban';
      break;
    case 9:
      month = 'Ramadan';
      break;
    case 10:
      month = 'Shawwal';
      break;
    case 11:
      month = 'Dhu al-Qi’dah';
      break;
    case 12:
      month = 'Dhu al-Hijjah';
      break;
    default:
      month = '';
  }

  const result = `${hijri.hd} ${month} ${hijri.hy}`;
  // console.log(hijri.hm)

  return result;
};

const convertToDuration = secondsAmount => {
  const normalizeTime = time => (time.length === 1 ? `0${time}` : time);

  const SECONDS_TO_MILLISECONDS_COEFF = 1000;
  const MINUTES_IN_HOUR = 60;

  const milliseconds = secondsAmount * SECONDS_TO_MILLISECONDS_COEFF;

  const date = new Date(milliseconds);
  const timezoneDiff = date.getTimezoneOffset() / MINUTES_IN_HOUR;
  const dateWithoutTimezoneDiff = addHours(date, timezoneDiff);

  const hours = normalizeTime(String(getHours(dateWithoutTimezoneDiff)));
  const minutes = normalizeTime(String(getMinutes(dateWithoutTimezoneDiff)));
  const seconds = normalizeTime(String(getSeconds(dateWithoutTimezoneDiff)));

  const hoursOutput = hours !== '00' ? `${hours}:` : '';

  return `${hoursOutput}${minutes}:${seconds}`;
};

const capitalise = name => name.charAt(0).toUpperCase() + name.slice(1);

const dayOfWeekName = day => {
  let result;

  switch (day) {
    case 0:
      result = 'Monday';
      break;
    case 1:
      result = 'Tuesday';
      break;
    case 2:
      result = 'Wednesday';
      break;
    case 3:
      result = 'Thursday';
      break;
    case 4:
      result = 'Friday';
      break;
    case 5:
      result = 'Saturday';
      break;
    case 6:
      result = 'Sunday';
      break;
    default:
      result = '';
  }
  return result;
};

const monthName = month => {
  let result;

  switch (month) {
    case 0:
      result = 'January';
      break;
    case 1:
      result = 'February';
      break;
    case 2:
      result = 'March';
      break;
    case 3:
      result = 'April';
      break;
    case 4:
      result = 'May';
      break;
    case 5:
      result = 'June';
      break;
    case 6:
      result = 'July';
      break;
    case 7:
      result = 'August';
      break;
    case 8:
      result = 'September';
      break;
    case 9:
      result = 'October';
      break;
    case 10:
      result = 'November';
      break;
    case 11:
      result = 'December';
      break;
    default:
      result = '';
  }
  return result;
};

export default { wordCount };
export { prependZero, wordCount, hijriConvert, hijriDate, convertToDuration, capitalise, dayOfWeekName, monthName };
