module.exports = {
  content: ['./src/**/*.html', './src/**/*.svg', './src/**/*.js', './src/**/*.svelte'],
  extractors: [
    {
      extensions: ['html', 'svg', 'js', 'svelte'],
      extractor: class TailwindExtractor {
        static extract(content) {
          // return content.match(/[A-Za-z0-9-_:\/]+/g) || []
          return content.match(/[A-Za-z0-9-_:/]+/g) || [];
          // return content => content.match(/[\w-/:]+(?<!:)/g) || []
        }
      },
    },
  ],
};
