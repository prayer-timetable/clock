// ***************** */
//  BASE STYLE       */
// ***************** */
const mainColor = '#fff';
const backgroundColor = '#324090'; // choose null if non-transparent background image should be used
const highlightColor = '#ee3300';

const appStyle = `background-color:${backgroundColor};color:${mainColor};fill:${mainColor}`;

// ***************** */
//  OVERRIDE STYLES  */
// ***************** */

//* **  HEADER **/
const headerStyle = '';
const logoStyle = '';

//* **  CLOCK **/
const clockStyle = '';

//* **  PRAYERS **/
const prayersStyle = '';
const prayerRowStyle = '';
const prayerHeaderStyle = '';
const prayerMainStyle = '';
const prayerNextStyle = '';

//* **  MESSAGE **/
const messageStyle = '';
const messageTextStyle = '';
const messageRefStyle = '';

//* **  COUNTDOWN **/
const countdownStyle = '';
const barStyle = '';
const barBgStyle = '';

//* **  FOOTER **/
const footerStyle = '';

export default appStyle;
export {
  mainColor,
  highlightColor,
  backgroundColor,
  appStyle,
  headerStyle,
  logoStyle,
  clockStyle,
  prayersStyle,
  prayerRowStyle,
  prayerHeaderStyle,
  prayerMainStyle,
  prayerNextStyle,
  messageStyle,
  messageTextStyle,
  messageRefStyle,
  countdownStyle,
  barStyle,
  barBgStyle,
  footerStyle,
};
