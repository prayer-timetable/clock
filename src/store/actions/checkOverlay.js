import { toDate, getYear, getMonth, getDate, getDay, addHours, setDay, isWithinInterval, addMinutes } from 'date-fns';

import { hijriConvert, capitalise } from '../../helpers/func';
import defsettings from '../../config/settings';

const checkOverlay = (settings, current) => {
  const now = new Date();

  // console.log(getDay(now))
  // JUMMUAH
  const jummuah =
    getDay(now) === 5
      ? setDay(
          toDate(new Date(getYear(now), getMonth(now), getDate(now), settings.jummuahtime[0], settings.jummuahtime[1])),
          5
        )
      : null;
  const isJummuah = jummuah
    ? isWithinInterval(now, {
        start: jummuah,
        end: addHours(jummuah, 1),
      })
    : false;
  // console.log(isJummuah)

  // TARAWEEH
  const taraweeh =
    hijriConvert(now).hm === 9
      ? setDay(
          toDate(new Date(getYear(now), getMonth(now), getDate(now), settings.jummuahtime[0], settings.jummuahtime[1])),
          5
        )
      : null;
  const isTaraweeh = taraweeh
    ? isWithinInterval(now, {
        start: taraweeh,
        end: addHours(taraweeh, 2),
      })
    : false;

  // Prayer Time
  // console.log(current);
  const prayer = defsettings.jamaahOverlay ? current.jtime : null;
  const isPrayer = () => {
    if (!prayer) return false; // if prayer is not defined in defsetting
    if (current.index === 1) return false; // if shurooq
    if (getDay(now) === 5 && current.index === 2) return false; // friday day of week, dhuhr

    if (
      isWithinInterval(now, {
        start: prayer,
        end: addMinutes(prayer, 10),
      })
    )
      return true; // if is within jamaah time and jamaah time + 10 mins

    return false; // otherwise false
  };

  // console.log(isPrayer);

  // TEST START
  const testEvent = false; // enable/disable test
  const DOW = 4;
  const MINUTES = 500;
  if (testEvent) {
    const test =
      getDay(now) === DOW // day of week
        ? setDay(
            toDate(new Date(getYear(now), getMonth(now), getDate(now), 14, 0)), // hours, mins
            DOW // day of week
          )
        : null;
    const isTest = test
      ? isWithinInterval(now, {
          start: test,
          // end: addHours(test, 4), // test + hours
          end: addMinutes(test, MINUTES), // test + minutes
        })
      : false;
    // console.log('isJummuah:', isJummuah, 'isTaraweeh:', isTaraweeh, 'isTest:', isTest);

    if (isTest) {
      return `${capitalise(current.name)} jamaah @ ${current.jtime.getHours()}:${current.jtime.getMinutes()}`;
    }
  }
  // END TEST

  if (isJummuah) {
    return 'Jummuah';
  }
  if (isTaraweeh) {
    return 'Taraweeh';
  }
  if (isPrayer()) {
    return `${capitalise(current.name)} jamaah @ ${current.jtime.getHours()}:${current.jtime.getMinutes()}`;
  }

  return null;
};

export default checkOverlay;
